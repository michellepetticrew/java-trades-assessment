package com.citi.training.trades.dao;

import java.util.List;

import com.citi.training.trades.model.Trades;

public interface TradesDao {
	
	Trades create(Trades trades);

	List<Trades> findAll();

	Trades findById(int id);

	void deleteById(int id);
	
}