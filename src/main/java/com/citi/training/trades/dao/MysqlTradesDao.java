package com.citi.training.trades.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trades;

@Component
public class MysqlTradesDao implements TradesDao{
	
	@Autowired
	JdbcTemplate tpl;
	
	public Trades create(Trades trades) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement(
						"INSERT INTO trade (stock, price, volume) VALUES (?, ?, ?)", 
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, trades.getStock());
				ps.setDouble(2, trades.getPrice());
				ps.setInt(3, trades.getVolume());
				return ps;
			}
		}, keyHolder);
		trades.setId(keyHolder.getKey().intValue());
		return trades;
	}
	
	public List<Trades> findAll() {
		return tpl.query("SELECT id, stock, price, volume FROM trade;", new TradeMapper());
	}
	
	public Trades findById(int id) {
		List<Trades> trades = tpl.query(
				"SELECT id, stock, price, volume FROM trade WHERE id=?",
				new Object[] {id},
				new TradeMapper()
		);
		
		if (trades.size() <= 0) {
			throw new TradeNotFoundException("Trade with id=[" + id + "] not found");
		}
		return trades.get(0);
	}
	
	public void deleteById(int id) {
		findById(id);
		
		tpl.update("DELETE FROM trade WHERE id=?", id);
	}
	
	private static final class TradeMapper implements RowMapper<Trades> {

		public Trades mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Trades(rs.getInt("id"), rs.getString("stock"), rs.getDouble("price"), rs.getInt("volume"));
		}
	}
	
}
