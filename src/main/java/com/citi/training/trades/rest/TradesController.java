package com.citi.training.trades.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trades.model.Trades;
import com.citi.training.trades.service.TradesService;

/**
 * The REST Interface class for {@link com.citi.training.trades.model.Trades} domain object.
 * @author Administrator
 * @see Trades
 *
 */

@RestController
@RequestMapping("/trades")
public class TradesController {

	@Autowired
	private TradesService tradesService;

	private static final Logger LOG = LoggerFactory.getLogger(TradesController.class);

	/**
	 * Create {@link com.citi.training.trades.model.Trades} by entering a trade name, price and volume.  
	 * @param trades The trade that is to be created
	 * @return {@link com.citi.training.trades.model.Trades}
	 */
	@RequestMapping(method = RequestMethod.POST, 
					consumes = MediaType.APPLICATION_JSON_VALUE, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Trades> create(@RequestBody Trades trades) {
		LOG.debug("create was called, trade: " + trades);
		return new ResponseEntity<Trades>(tradesService.create(trades), HttpStatus.CREATED);
	}

	/**
	 * Display all {@link com.citi.training.trades.model.Trades}
	 * @return the full list of trades
	 */
	@RequestMapping(method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Trades> findAll() {
		LOG.debug("findAll() was called");
		return tradesService.findAll();
	}
	
	/**
	 * Find a {@link com.citi.training.trades.model.Trades} by its ID
	 * @param id The id of the trade to find
	 * @return {@link com.citi.training.trades.model.Trades} that was found or HTTP 404
	 */
	@RequestMapping(value = "/{id}", 
					method = RequestMethod.GET, 
					produces = MediaType.APPLICATION_JSON_VALUE)
	public Trades findById(@PathVariable int id) {
		LOG.debug("findById() was called, trade id: " + id);
		return tradesService.findById(id);
	}
	
	/**
	 * Deleting a {@link com.citi.training.trades.model.Trades} by its ID
	 * @param id The id of the trade to delete.
	 */
	@RequestMapping(value="/{id}", 
					method=RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public @ResponseBody void deleteById(@PathVariable int id) {
    	LOG.debug("deleteById was called, trade id: " + id);
        tradesService.deleteById(id);
    }

}
