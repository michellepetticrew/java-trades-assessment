package com.citi.training.trades.rest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.model.Trades;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@Transactional
@ActiveProfiles("h2")
public class TradesControllerIntegrationTests {
	
private static final Logger logger = LoggerFactory.getLogger(TradesControllerTests.class);
	
	@Autowired
    private TestRestTemplate restTemplate;
	
	@Test
    public void getTrade_returnsTrade() {
        restTemplate.postForEntity("/trades",
                                   new Trades(5, "TRADE", 5.69, 9), Trades.class);

        ResponseEntity<List<Trades>> getAllResponse = restTemplate.exchange(
                                "/trades",
                                HttpMethod.GET,
                                null,
                                new ParameterizedTypeReference<List<Trades>>(){});

        logger.info("getAllTrades response: " + getAllResponse.getBody());

        assertEquals(HttpStatus.OK, getAllResponse.getStatusCode());
        assertTrue(getAllResponse.getBody().get(0).getStock().equals("TRADE"));
        assertEquals(getAllResponse.getBody().get(0).getPrice(), 5.69, 0.0001);
        assertEquals(getAllResponse.getBody().get(0).getVolume(), 9);
        
        //restTemplate.delete("/trades/" + getAllResponse.getBody().get(0).getId());
	}

}
