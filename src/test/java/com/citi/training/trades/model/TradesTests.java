package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradesTests {
	
	private int testId = 3;
	private String testStock = "GOOGL";
	private double testPrice = 6.99;
	private int testVolume = 5;
	
	@Test
	public void test_Trades_constructor() {
		Trades testTrade = new Trades(testId, testStock, testPrice, testVolume);
		
		//System.out.println(testTrade);
		
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock());
		assertEquals(testPrice, testTrade.getPrice(), 0.001);
		assertEquals(testVolume, testTrade.getVolume());
	}
	
	@Test
	public void test_Trades_toString() {
		String testTradeString = new Trades(testId, testStock, testPrice, testVolume).toString();
		
		//System.out.println(testTradeString);
		
		assertTrue(testTradeString.contains((new Integer(testId)).toString()));
		assertTrue(testTradeString.contains(testStock));
		assertTrue(testTradeString.contains(String.valueOf(testPrice)));
		assertTrue(testTradeString.contains(new Integer(testVolume).toString()));
	}
	
}
