package com.citi.training.trades.dao;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.trades.exceptions.TradeNotFoundException;
import com.citi.training.trades.model.Trades;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("h2")
public class MysqlTradesDaoTests {

	@Autowired
	MysqlTradesDao mysqlTradesDao;
	
	@Test
	@Transactional
	public void test_createAndFindAllTrades() {
		mysqlTradesDao.create(new Trades(-1, "GOGGL", 10.0, 5));
		
		System.out.println(mysqlTradesDao.findAll());
		
		assertTrue(mysqlTradesDao.findAll().size() >= 1);
	}
	
	@Test
	@Transactional
	public void test_createAndFindById() {
		Trades trades = new Trades(2, "MICRO", 10.0, 6);
		trades = mysqlTradesDao.create(trades);
		
		assertThat(mysqlTradesDao.findById(trades.getId()).equals(trades));
 
	}
	
	@Test (expected = TradeNotFoundException.class)
	@Transactional
	public void test_createAndDeleteById() {
		mysqlTradesDao.create(new Trades(4, "APPLE", 10.0, 9));
		
		mysqlTradesDao.deleteById(1);
		
		assertEquals(mysqlTradesDao.findById(1), 1);
		
	}
	
}
